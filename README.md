# start mongodb container
docker-compose up -d

# check mongodb status
docker ps -a

# interact with mongodb
docker exec -it mongodb bash

# switch the database
use opticare

# list collections
show collections

# query collection data
db.counters.find().pretty()

# remove mongodb container
docker-compose down

# remove local mongo image
docker rmi mongo
